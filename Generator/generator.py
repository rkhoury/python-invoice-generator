'''@author: Rayyan Khoury
generator.py is in charge of document generation. It takes the user inputs from the GUI, handles the arguments cleanly, and posts them to a DOCX and PDF documents.
The code is modular and new inputs can easily be added to the generator. 
'''

from docx import Document
from docx.shared import Inches
from docx.shared import Pt
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
import sys
import os
from win32com.client import Dispatch
from time import sleep
import datetime

class Generator:
	# Takes the inputs from the GUI and saves them within the class to be used in later functions
    def __init__(self, name, company, creditor, terms, duedate, hourlyrate, discount, itemlist, font):
        self.name = name
        self.company = company
        self.creditor  = creditor
        self.terms = terms
        self.duedate = duedate
        self.hourlyrate = hourlyrate
        self.discount = discount
        self.itemlist = itemlist
        self.font = font
        self.totalBalance = 0.0
        self.getTotalBalance()
        self.create()
    
	# Create handles the ordering of function calling and the initial creation of the document
	# It accounts for file and path errors implicitly
    def create(self):
    
		# Document creation and template generation
        if not os.path.exists('Invoices'):
            os.makedirs('Invoices')
        self.date = datetime.datetime.now()
        self.document = Document(os.path.abspath('default.docx'))
        style = self.document.styles['Normal']
        font = style.font
        font.name = self.font
		
		# Calls the functions required to start editing the template document
        x = self.headerTable()
        y = self.invoiceTable()
        z = self.terms
        a = self.save()

	# Header Table is in charge of displaying everything necessary to distinguish the document as an invoice.
	# It adds the logo inputted by the user and created the header with the Invoice statement.
    def headerTable(self):
        #First Row
        headertable = self.document.add_table(rows=3, cols=3)
        headercells = headertable.rows[0].cells
        paragraph = headercells[0].paragraphs[0]
        run = paragraph.add_run()
        run.add_picture('logo.jpg', width=Inches(2))
        paragraph = headercells[2].paragraphs[0]
        paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT  # @UndefinedVariable
        run = paragraph.add_run('Invoice')
        font = run.font
        font.size = Pt(30)
        font.bold = True
		
        # Second Row
        headercells = headertable.rows[1].cells
        paragraph = headercells[0].paragraphs[0]
        run = paragraph.add_run('\n' + self.name + self.company)
        font = run.font
        font.bold = True
        paragraph = headercells[2].paragraphs[0]
        run = paragraph.add_run()
        paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT# @UndefinedVariable
        run = paragraph.add_run('Date: ' + ("%s-%s-%s" % (self.date.day, self.date.month, self.date.year)))
        run = paragraph.add_run('\nTerms: ' + str(self.terms))
        run = paragraph.add_run('\nDue Date: ' + str(self.duedate))
		
        #Third Row
        headercells = headertable.rows[2].cells
        paragraph = headercells[0].paragraphs[0]
        run = paragraph.add_run()
        paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT# @UndefinedVariable
        run = paragraph.add_run('Bill to: ')
        font = run.font
        font.size = Pt(12)
        font.bold = True
        run = paragraph.add_run(str(self.creditor))
        paragraph = headercells[2].paragraphs[0]
        run = paragraph.add_run()
        paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT# @UndefinedVariable
        run = paragraph.add_run('Total Balance: ' + str(self.totalBalance))

	# Invoice Table is in charge of taking all inputs no matter how many inputs are given and generating a table to contain the information.
    def invoiceTable(self):
        
        table = self.document.add_table(rows=1, cols=4)
        table.style = "Light Shading Accent 1"
        hdr_cells = table.rows[0].cells
        hdr_cells[0].text = 'Description'
        hdr_cells[1].text = 'Quantity'
        hdr_cells[2].text = 'Rate/hr'
        hdr_cells[3].text = 'Amount'
        for x in self.itemlist:
            row_cells = table.add_row().cells
            row_cells[0].text = str(x[0])
            row_cells[1].text = str(x[1])
            row_cells[2].text = str(self.hourlyrate)
            row_cells[3].text = str(float(float(x[1]) * float(self.hourlyrate)))

	# Handles the terms of the payment.
    def terms(self):
        terms = self.document.add_paragraph()
        terms.add_run('Terms and Conditions:').bold = True
    
	# Helper function to save the document in an Invoices folder. 
    def save(self):
        self.document.save(str('Invoices\\Invoice ' + ("%s %s-%s-%s" % (self.creditor, self.date.day, self.date.month, self.date.year)) + '.docx'))
        wdFormatPDF = 17
        in_file = os.path.abspath(str('Invoices\\Invoice ' + ("%s %s-%s-%s" % (self.creditor, self.date.day, self.date.month, self.date.year)) + '.docx'))
        out_file = os.path.abspath(str('Invoices\\Invoice ' + ("%s %s-%s-%s" % (self.creditor, self.date.day, self.date.month, self.date.year)) + '.pdf'))
        word = Dispatch('Word.Application')
        doc = word.Documents.Open(in_file)
        doc.SaveAs(out_file, FileFormat=wdFormatPDF)
        doc.Close()
        word.Quit()
    
	# Calculates the total balance based on the number of hours and the hourly rate.
    def getTotalBalance(self):
        # tuple of description, hours
        for x in self.itemlist:
            self.totalBalance += float(float(x[1]) * float(self.hourlyrate))
            