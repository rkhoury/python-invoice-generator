'''@author: Rayyan Khoury
welcome.py is in charge of the generation of the welcome screen used to take the intial user's information and store them on disk
'''
import os
import PySide.QtGui
import PySide.QtCore
import GUI.generateUI
from shutil import copyfile

class Welcome(PySide.QtGui.QWidget):
    screen = None
    desktopwidth = None
    desktopheight = None
    calculatedHeight = 20
    calculatedWidth = 40
    currentHeight = 20
    widgets = []
    def __init__(self):
        super(Welcome, self).__init__()
        self.initUI()

	# Essential Function to set all the GUI features of the window in order	
    def initUI(self):
        self.introText() 
        self.inputBox()
        self.submitButton()
        self.fileUpload()
        self.calculateGeometry()
        self.setWidgetPositions()
        self.setWindowProperties()
        self.show()
		
	# A Function designed to make sure that all widgets are centered based on their calculated size
    def setWidgetPositions(self):
        for widget in self.widgets:
            widget.move(self.calculatedWidth * 0.5 - 0.5 * widget.sizeHint().width(), self.currentHeight)
            self.currentHeight += widget.sizeHint().height() + 20
			
	# Calculates the necessary geometry for the different widgets to fit on the screen
    def calculateGeometry(self):
        for widget in self.widgets:
            self.calculatedHeight += widget.sizeHint().height() + 20
            self.calculatedWidth = max(widget.sizeHint().width() + 40, self.calculatedWidth)
			
	# Based on the prior widget size calculations, calculates an appropriate size for the window to appear on the screen
    def setWindowProperties(self):
         
        self.setFixedSize(self.calculatedWidth, self.calculatedHeight)
        self.setWindowTitle('Welcome')
        self.setWindowIcon(PySide.QtGui.QIcon('logo.jpg'))
    
	# Generates the intro text
    def introText(self):
        self.introText = PySide.QtGui.QLabel(self)
        self.introText.setFrameStyle(PySide.QtGui.QFrame.Plain)
        self.introText.setText("To help us personalize your invoices, please enter the following information:")
        self.introText.setAlignment(PySide.QtCore.Qt.AlignTop | PySide.QtCore.Qt.AlignLeft)
        self.widgets.append(self.introText)
		
	# Creates the input box asking for user information
    def inputBox(self):
        self.inputs = PySide.QtGui.QGroupBox(self)
        
        self.inputs.companyNameLabel = PySide.QtGui.QLabel('Company Name:')
        self.inputs.companyNameData = PySide.QtGui.QLineEdit()
        self.inputs.personalNameLabel = PySide.QtGui.QLabel('Personal Name:')
        self.inputs.personalNameData = PySide.QtGui.QLineEdit()
        self.inputs.usernameLabel = PySide.QtGui.QLabel('User Name:')
        self.inputs.usernameData = PySide.QtGui.QLineEdit()
        self.inputs.passwordLabel = PySide.QtGui.QLabel('Password:')
        self.inputs.passwordData = PySide.QtGui.QLineEdit()
        self.inputs.passwordData.setEchoMode(PySide.QtGui.QLineEdit().PasswordEchoOnEdit)
        self.inputs.logoLabel = PySide.QtGui.QLabel('Company Logo:')
        self.inputs.logoData = PySide.QtGui.QPushButton('Select Image', self)
        self.inputs.logoImage = None
        
        self.inputBox = PySide.QtGui.QGridLayout()
        self.inputBox.setSpacing(2)
        
        self.leftInputs = PySide.QtGui.QGroupBox()
        self.leftLayout = PySide.QtGui.QVBoxLayout()
        self.rightInputs = PySide.QtGui.QGroupBox()
        self.rightLayout = PySide.QtGui.QVBoxLayout()
        
        self.leftLayout.addWidget(self.inputs.companyNameLabel)
        self.leftLayout.addWidget(self.inputs.personalNameLabel)
        self.leftLayout.addWidget(self.inputs.usernameLabel)
        self.leftLayout.addWidget(self.inputs.passwordLabel)
        self.leftLayout.addWidget(self.inputs.logoLabel)
        self.leftInputs.setLayout(self.leftLayout)
        
        self.rightLayout.addWidget(self.inputs.companyNameData)
        self.rightLayout.addWidget(self.inputs.personalNameData)
        self.rightLayout.addWidget(self.inputs.usernameData)
        self.rightLayout.addWidget(self.inputs.passwordData)
        self.rightLayout.addWidget(self.inputs.logoData)
        self.rightInputs.setLayout(self.rightLayout)
        
        self.inputBox.addWidget(self.leftInputs, 0, 0)
        self.inputBox.addWidget(self.rightInputs, 0, 1)
        
        self.inputs.setLayout(self.inputBox)
		
        self.widgets.append(self.inputs)
		
	# Once all fields are filled, this button tells the backend to store the user's information
    def submitButton(self):
        self.btn = PySide.QtGui.QPushButton('Save Information', self)
        self.widgets.append(self.btn)
        self.btn.clicked.connect(self.submitClicked)

	# Helper for logo button
    def fileUpload(self):
        self.connect(self.inputs.logoData, PySide.QtCore.SIGNAL('clicked()'), self.uploadFile)
		
	# Once the submit button is clicked, all errors are handled here and the user is prompted if there is missing information
    def submitClicked(self):
		# Checks if fields are complete
        if not self.inputs.companyNameData.text() \
            or not self.inputs.personalNameData.text() \
            or not self.inputs.usernameData.text() \
            or not self.inputs.passwordData.text() \
            or not self.inputs.logoImage:
                self.noDetails = PySide.QtGui.QMessageBox.warning(self, 'Incomplete',
                                            "Fill all fields",
                                            PySide.QtGui.QMessageBox.Close)
		# Passes error checks
        else:
            f = open('userdata.txt', 'w')
            f.write(self.inputs.companyNameData.text()+"\n")
            f.write(self.inputs.personalNameData.text()+"\n")
            f.write(self.inputs.usernameData.text()+"\n")
            f.write(self.inputs.passwordData.text()+"\n")
            f.close()
            self.close()
            self.screen = GUI.generateUI.GenerateUI()
    
	# Handle's storing the logo locally and setting the Icon of the program to the inputted logo
    def uploadFile(self):
        filename = PySide.QtGui.QFileDialog.getOpenFileName(self, 'Open File', '.', "Image Files (*.jpg)")
        self.inputs.logoData.setStyleSheet("background-color: grey")
        self.inputs.logoData.setText('Image Selected')
        self.inputs.logoImage = 'yes'
        copyfile(self.clean_path(filename[0]), self.clean_path(os.getcwd())+'\\logo'+os.path.splitext(filename[0])[1])
        self.setWindowIcon(PySide.QtGui.QIcon(filename[0]))
    
	# Helper function to make sure path is correct for Windows
    def clean_path(self, path):
        path = path.replace('/',os.sep).replace('\\',os.sep)
        if os.sep == '\\' and '\\\\?\\' not in path:
            relative_levels = len([directory for directory in path.split(os.sep) if directory == '..'])
            cwd = [directory for directory in os.getcwd().split(os.sep)] if ':' not in path else []
            path = '\\\\?\\' + os.sep.join(cwd[:len(cwd)-relative_levels]\
                                           + [directory for directory in path.split(os.sep) if directory!=''][relative_levels:])
            return path