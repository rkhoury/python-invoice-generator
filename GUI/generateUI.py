'''@author: Rayyan Khoury
generateUI.py is in charge of the generation of windows used by the user. Geometry is set based on different screen sizes, and widget dimensions are calculated
to make sure that everything fits on any screen cleanly. All labels are generated here and inputs are stored and passed to the generator class for use.
'''

import PySide.QtGui
import Generator.generator
from tkinter import Tk, font

class GenerateUI(PySide.QtGui.QWidget):
    screen = None
    desktopwidth = None
    desktopheight = None
    calculatedHeight = 20
    calculatedWidth = 40
    currentHeight = 20
    widgets = []
    def __init__(self):
        super(GenerateUI, self).__init__()
        f = open('userdata.txt', 'r')
        self.lines=f.readlines()
        self.company = self.lines[0]
        self.name = self.lines[1]
        f.close()
        self.initUI()
    
	# Essential Function to set all the GUI features of the window in order
    def initUI(self):
        self.fontSelector()
        self.inputGeneralInformation()
        self.inputInvoiceInformationLimited()
        self.submitButton()
        
        self.calculateGeometry()
        self.setWidgetPositions()
        self.setWindowProperties()
        
        self.show()
    
	# A Function designed to make sure that all widgets are centered based on their calculated size
    def setWidgetPositions(self):
        for widget in self.widgets:
            widget.move(self.calculatedWidth * 0.5 - 0.5 * widget.sizeHint().width(), self.currentHeight)
            self.currentHeight += widget.sizeHint().height() + 20
    
	# Calculates the necessary geometry for the different widgets to fit on the screen
    def calculateGeometry(self):
        for widget in self.widgets:
            self.calculatedHeight += widget.sizeHint().height() + 20
            self.calculatedWidth = max(widget.sizeHint().width() + 40, self.calculatedWidth)
    
	# Based on the prior widget size calculations, calculates an appropriate size for the window to appear on the screen
    def setWindowProperties(self):
        self.setFixedSize(self.calculatedWidth, self.calculatedHeight)
        self.setWindowTitle(self.name + '\'s Invoice Generator')
        self.setWindowIcon(PySide.QtGui.QIcon('logo.jpg'))
    
	# Adds the font selector for the user to choose what font to generate their document in
	# Finds and uses the fonts installed in the Font directory
    def fontSelector(self):
        root = Tk()
        f = [x for x in sorted(font.families(), key=str.lower) if not x.startswith('@')]
        self.fonts = PySide.QtGui.QGroupBox(self)
               
        self.fonts.fontsLabel = PySide.QtGui.QLabel('Choose a font:')
        self.fonts.fontsList = PySide.QtGui.QFontComboBox(self)
        
        self.fontsBox = PySide.QtGui.QGridLayout()
        self.fontsBox.setSpacing(2)
        self.leftInputs = PySide.QtGui.QGroupBox()
        self.leftLayout = PySide.QtGui.QVBoxLayout()
        self.rightInputs = PySide.QtGui.QGroupBox()
        self.rightLayout = PySide.QtGui.QVBoxLayout()
        
        self.leftLayout.addWidget(self.fonts.fontsLabel)        
        self.leftInputs.setLayout(self.leftLayout)
        
        self.rightLayout.addWidget(self.fonts.fontsList)
        self.rightInputs.setLayout(self.rightLayout)
        
        self.fontsBox.addWidget(self.leftInputs, 0, 0)
        self.fontsBox.addWidget(self.rightInputs, 0, 1)
        self.fonts.setLayout(self.fontsBox)
		
        self.widgets.append(self.fonts)
    
	# General input is handled by this widget and contains non specific information in regards to the company you are invoicing
    def inputGeneralInformation(self):
        self.inputs = PySide.QtGui.QGroupBox(self)
        self.inputs.validator = PySide.QtGui.QDoubleValidator()
        self.inputs.creditorLabel = PySide.QtGui.QLabel('Creditor:')
        self.inputs.creditorData = PySide.QtGui.QLineEdit()
        self.inputs.termsLabel = PySide.QtGui.QLabel('Payment Structure:')
        self.inputs.termsData = PySide.QtGui.QLineEdit()
        self.inputs.dueDateLabel = PySide.QtGui.QLabel('Due Date:')
        self.inputs.dueDateData = PySide.QtGui.QPushButton('Select Date', self)
        self.inputs.dueDateData.clicked.connect(self.dateButtonClicked)
        self.inputs.dueDate = None
        self.inputs.hourlyRateLabel = PySide.QtGui.QLabel('Hourly Rate ($):')
        self.inputs.hourlyRateData = PySide.QtGui.QLineEdit()
        self.inputs.hourlyRateData.setValidator(self.inputs.validator)
        self.inputs.discountLabel = PySide.QtGui.QLabel('Discount, good faith (%):')
        self.inputs.discountData = PySide.QtGui.QLineEdit()
        self.inputs.discountData.setValidator(self.inputs.validator)
        
        self.inputBox = PySide.QtGui.QGridLayout()
        self.inputBox.setSpacing(2)
        self.leftInputs = PySide.QtGui.QGroupBox()
        self.leftLayout = PySide.QtGui.QVBoxLayout()
        self.rightInputs = PySide.QtGui.QGroupBox()
        self.rightLayout = PySide.QtGui.QVBoxLayout()
        
        self.leftLayout.addWidget(self.inputs.creditorLabel)
        self.leftLayout.addWidget(self.inputs.termsLabel)
        self.leftLayout.addWidget(self.inputs.dueDateLabel)
        self.leftLayout.addWidget(self.inputs.hourlyRateLabel)
        self.leftLayout.addWidget(self.inputs.discountLabel)
        self.leftInputs.setLayout(self.leftLayout)
        
        self.rightLayout.addWidget(self.inputs.creditorData)
        self.rightLayout.addWidget(self.inputs.termsData)
        self.rightLayout.addWidget(self.inputs.dueDateData)
        self.rightLayout.addWidget(self.inputs.hourlyRateData)
        self.rightLayout.addWidget(self.inputs.discountData)
        self.rightInputs.setLayout(self.rightLayout)
        
        self.inputBox.addWidget(self.leftInputs, 0, 0)
        self.inputBox.addWidget(self.rightInputs, 0, 1)
        self.inputs.setLayout(self.inputBox)
        
        self.widgets.append(self.inputs)
    
	# The specific work you are invoicing for is generated here and sized appropriately
    def inputInvoiceInformation(self):
        self.invoices = PySide.QtGui.QGroupBox(self)
        self.invoices.addLabel = PySide.QtGui.QLabel('Add Item:')
        self.invoices.addButton = PySide.QtGui.QPushButton('+', self)
        self.invoices.addButton.clicked.connect(self.addButtonClicked)
        self.invoices.descriptionLabel = PySide.QtGui.QLabel('Description')
        self.invoices.hoursLabel = PySide.QtGui.QLabel('Hours')
        
        self.invoices.inputBox = PySide.QtGui.QGridLayout()
        self.invoices.inputBox.setSpacing(2)
        self.invoices.leftInputs = PySide.QtGui.QGroupBox()
        self.invoices.leftLayout = PySide.QtGui.QVBoxLayout()
        self.invoices.rightInputs = PySide.QtGui.QGroupBox()
        self.invoices.rightLayout = PySide.QtGui.QVBoxLayout()
        
        self.invoices.leftLayout.addWidget(self.invoices.addLabel)
        self.invoices.leftLayout.addWidget(self.invoices.descriptionLabel)
        self.invoices.leftInputs.setLayout(self.invoices.leftLayout)
        self.invoices.rightLayout.addWidget(self.invoices.addButton)
        self.invoices.rightLayout.addWidget(self.invoices.hoursLabel)
        self.invoices.rightInputs.setLayout(self.invoices.rightLayout)
        
        self.invoices.inputBox.addWidget(self.invoices.leftInputs, 0, 0)
        self.invoices.inputBox.addWidget(self.invoices.rightInputs, 0, 1)
        self.invoices.setLayout(self.invoices.inputBox)
        self.invoices.move(0.5*self.frameGeometry().width() - 0.5*self.invoices.sizeHint().width(), 160)
    
	# The specific work you are invoicing for is generated here and sized appropriately
    def inputInvoiceInformationLimited(self):
        self.invoices = PySide.QtGui.QGroupBox(self)
        
        self.invoices.descriptionLabel = PySide.QtGui.QLabel('Description of work: ')
        self.invoices.hoursLabel = PySide.QtGui.QLabel('Hours: ')
        
        self.invoices.validator = PySide.QtGui.QDoubleValidator()
        
        self.invoices.data1desc = PySide.QtGui.QLineEdit()
        self.invoices.data1desc.setFixedWidth(400)
        self.invoices.data1hours = PySide.QtGui.QLineEdit()
        self.invoices.data1hours.setFixedWidth(40)
        self.invoices.data1hours.setValidator(self.invoices.validator)
        self.invoices.data2desc = PySide.QtGui.QLineEdit()
        self.invoices.data2desc.setFixedWidth(400)
        self.invoices.data2hours = PySide.QtGui.QLineEdit()
        self.invoices.data2hours.setFixedWidth(40)
        self.invoices.data2hours.setValidator(self.invoices.validator)
        self.invoices.data3desc = PySide.QtGui.QLineEdit()
        self.invoices.data3desc.setFixedWidth(400)
        self.invoices.data3hours = PySide.QtGui.QLineEdit()
        self.invoices.data3hours.setFixedWidth(40)
        self.invoices.data3hours.setValidator(self.invoices.validator)
        self.invoices.data4desc = PySide.QtGui.QLineEdit()
        self.invoices.data4desc.setFixedWidth(400)
        self.invoices.data4hours = PySide.QtGui.QLineEdit()
        self.invoices.data4hours.setFixedWidth(40)
        self.invoices.data4hours.setValidator(self.invoices.validator)
        self.invoices.data5desc = PySide.QtGui.QLineEdit()
        self.invoices.data5desc.setFixedWidth(400)
        self.invoices.data5hours = PySide.QtGui.QLineEdit()
        self.invoices.data5hours.setFixedWidth(40)
        self.invoices.data5hours.setValidator(self.invoices.validator)
        
        self.invoices.inputBox = PySide.QtGui.QGridLayout()
        self.invoices.inputBox.setSpacing(2)
        self.invoices.leftInputs = PySide.QtGui.QGroupBox()
        self.invoices.leftLayout = PySide.QtGui.QVBoxLayout()
        self.invoices.rightInputs = PySide.QtGui.QGroupBox()
        self.invoices.rightLayout = PySide.QtGui.QVBoxLayout()
        
        self.invoices.leftLayout.addWidget(self.invoices.descriptionLabel)
        self.invoices.leftLayout.addWidget(self.invoices.data1desc)
        self.invoices.leftLayout.addWidget(self.invoices.data2desc)
        self.invoices.leftLayout.addWidget(self.invoices.data3desc)
        self.invoices.leftLayout.addWidget(self.invoices.data4desc)
        self.invoices.leftLayout.addWidget(self.invoices.data5desc)
        self.invoices.leftInputs.setLayout(self.invoices.leftLayout)
        
        self.invoices.rightLayout.addWidget(self.invoices.hoursLabel)
        self.invoices.rightLayout.addWidget(self.invoices.data1hours)
        self.invoices.rightLayout.addWidget(self.invoices.data2hours)
        self.invoices.rightLayout.addWidget(self.invoices.data3hours)
        self.invoices.rightLayout.addWidget(self.invoices.data4hours)
        self.invoices.rightLayout.addWidget(self.invoices.data5hours)
        self.invoices.rightInputs.setLayout(self.invoices.rightLayout)
        
        self.invoices.inputBox.addWidget(self.invoices.leftInputs, 0, 0)
        self.invoices.inputBox.addWidget(self.invoices.rightInputs, 0, 1)
        self.invoices.setLayout(self.invoices.inputBox)
        
        self.widgets.append(self.invoices)
    
	# Once all fields are filled, this button tells the backend to generate the invoice
    def submitButton(self):
        self.submitButton = PySide.QtGui.QPushButton('Generate Invoice', self)
        self.widgets.append(self.submitButton)
        self.submitButton.clicked.connect(self.submitClicked)
    
	# Creates the window to choose a date
    def dateButtonClicked(self):
        self.msgBox = PySide.QtGui.QMessageBox() 
        self.msgBox.setWindowTitle('Select a Date')
        self.msgBox.setWindowIcon(PySide.QtGui.QIcon('logo.jpg'))
        layout = self.msgBox.layout()
        layout.itemAtPosition( layout.rowCount() - 1, 0 ).widget().hide()
        self.msgBox.selector = PySide.QtGui.QCalendarWidget()
        layout.addWidget(self.msgBox.selector)
        self.msgBox.btn = PySide.QtGui.QPushButton('OK', self)
        self.msgBox.btn.move(0.5*self.frameGeometry().width() - 0.5*self.msgBox.btn.sizeHint().width(), 20)
        layout.addWidget(self.msgBox.btn)
        self.msgBox.btn.clicked.connect(self.dateSubmitted)
        self.msgBox.exec_()
    
	# A helper function to add work and hours
    def addButtonClicked(self):
        self.newWindow = PySide.QtGui.QMessageBox() 
        self.newWindow.setWindowTitle('Input Item')
        self.newWindow.setWindowIcon(PySide.QtGui.QIcon('logo.jpg'))
        layout = self.newWindow.layout()
        layout.itemAtPosition( layout.rowCount() - 1, 0 ).widget().hide()
        
        self.addInputs = PySide.QtGui.QGroupBox(self)
        self.addInputs.descriptionLabel = PySide.QtGui.QLabel('Description of work: ')
        self.addInputs.hoursLabel = PySide.QtGui.QLabel('Number of hours: ')
        self.addInputs.descriptionData = PySide.QtGui.QLineEdit()
        self.addInputs.hoursData = PySide.QtGui.QLineEdit()
        
        self.addInputsBox = PySide.QtGui.QGridLayout()
        self.addInputsBox.setSpacing(2)
        self.leftInputs = PySide.QtGui.QGroupBox()
        self.leftLayout = PySide.QtGui.QVBoxLayout()
        self.rightInputs = PySide.QtGui.QGroupBox()
        self.rightLayout = PySide.QtGui.QVBoxLayout()
        
        self.leftLayout.addWidget(self.addInputs.descriptionLabel)
        self.leftLayout.addWidget(self.addInputs.hoursLabel)
        self.leftInputs.setLayout(self.leftLayout)
        self.rightLayout.addWidget(self.addInputs.descriptionData)
        self.rightLayout.addWidget(self.addInputs.hoursData)
        self.rightInputs.setLayout(self.rightLayout)
        
        self.addInputsBox.addWidget(self.leftInputs, 0, 0)
        self.addInputsBox.addWidget(self.rightInputs, 0, 1)
        self.addInputs.setLayout(self.addInputsBox)
        
        self.newWindow.btn = PySide.QtGui.QPushButton('OK', self)
        self.newWindow.btn.move(0.5*self.frameGeometry().width() - 0.5*self.newWindow.btn.sizeHint().width(), 20)
        layout.addWidget(self.newWindow.btn)
        self.newWindow.btn.clicked.connect(self.dateSubmitted)
        self.newWindow.exec_()    
     
	# Once the user has submitted a Date, sets the panel as the date selected
    def dateSubmitted(self):
        self.inputs.dueDateData.setText(self.msgBox.selector.selectedDate().toString('dd-MM-yyyy'))
        self.inputs.dueDate = self.msgBox.selector.selectedDate().toString('dd-MM-yyyy')
        self.msgBox.close()
    
	# Once the generate button is clicked, all errors are handled here and the user is prompted if there is missing information
    def submitClicked(self):
		# Checks if fields are complete
        if not self.inputs.creditorData.text() \
            or not self.inputs.termsData.text() \
            or not self.inputs.dueDate \
            or not self.inputs.hourlyRateData.text() \
            or not self.inputs.discountData.text() \
            or not self.checkFieldsLimitedFilled():
                self.noDetails = PySide.QtGui.QMessageBox.warning(self, 'Incomplete',
                                            "Fill all fields",
                                            PySide.QtGui.QMessageBox.Close)
		# Makes sure where numbers are necessary other information is not input
        elif not self.isNumber(self.inputs.hourlyRateData.text()):
            self.notIntegers = PySide.QtGui.QMessageBox.warning(self, 'Error',
                                            "Hourly rate is not a valid number",
                                            PySide.QtGui.QMessageBox.Close)
        # Makes sure where numbers are necessary other information is not input    
        elif not self.isNumber(self.inputs.discountData.text()):
            self.notIntegers = PySide.QtGui.QMessageBox.warning(self, 'Error',
                                            "Discount rate is not a valid number",
                                            PySide.QtGui.QMessageBox.Close)
        # Passes error checks
        else:
            Generator.generator.Generator(self.name, self.company, self.inputs.creditorData.text(), self.inputs.termsData.text(), \
                                self.inputs.dueDate, self.inputs.hourlyRateData.text(), self.inputs.discountData.text(), self.createTuple(), self.fonts.fontsList.currentFont().rawName())

    # Creates the Tuple to be sent to the generator
    def createTuple(self):
        t = []
        t.append((self.invoices.data1desc.text(), self.invoices.data1hours.text(),))
        t.append((self.invoices.data2desc.text(), self.invoices.data2hours.text(),))
        t.append((self.invoices.data3desc.text(), self.invoices.data3hours.text(),))
        t.append((self.invoices.data4desc.text(), self.invoices.data4hours.text(),))
        t.append((self.invoices.data5desc.text(), self.invoices.data5hours.text(),))
        return t
    
	# A check for whether the fields are filled or not
    def checkFieldsLimitedFilled(self):
        if not self.invoices.data1desc.text() or not self.invoices.data2desc.text() \
        or not self.invoices.data3desc.text() or not self.invoices.data4desc.text() \
        or not self.invoices.data5desc.text() or not self.invoices.data1hours.text() \
        or not self.invoices.data2hours.text() or not self.invoices.data3hours.text() \
        or not self.invoices.data4hours.text() or not self.invoices.data5hours.text():
            return False
        return True;
    
	# A helper function to ascertain that the input is a float
    def isNumber(self, s):
        try: 
            float(s)
            return True
        except ValueError:
            return False