'''
@author: Rayyan Khoury
This is the main setup class designated to check whether user initialization has occured or not
'''

import sys
import os.path
import PySide.QtGui
import GUI.welcome
import GUI.generateUI

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
        
#Loads up the application and goes straight to invoice generation on a userdata document having been created
def main():
    app = PySide.QtGui.QApplication(sys.argv)
    if not (os.path.exists('userdata.txt')):
        t = GUI.welcome.Welcome()
    else:
        ex = GUI.generateUI.GenerateUI()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()